from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "image",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": list(hats.values())},
            encoder=HatEncoder,
            content_type="application/json",
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except KeyError:
            return JsonResponse(
                {"error": "Missing required fields in the request data."},
                status=400,
                content_type="application/json",
            )
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid location id"},
                status=400,
                content_type="application/json",
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            {"id": hat.id},
            status=200,
            content_type="application/json",
        )


@require_http_methods(["DELETE"])
def api_hat_delete(request, id):
    hat = get_object_or_404(Hat, id=id)

    if request.method == "DELETE":
        hat.delete()
        return JsonResponse(
            {"message": "Hat deleted successfully"},
            status=200,
            content_type="application/json",
        )
    else:
        return JsonResponse(
            {"error": "Method not allowed"},
            status=400,
            content_type="application/json",
        )
