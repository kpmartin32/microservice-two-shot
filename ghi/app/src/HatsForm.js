import React from 'react';

class HatsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: '',
            style: '',
            color: '',
            image: '',
            locations: [],
            selectedLocation: '',
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        console.log(data);
        delete data.locations;
        delete data.selectedLocation;
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            const cleared = {
                fabric: '',
                style: '',
                color: '',
                image: '',
                selectedLocation: '',
            };
            this.setState(cleared);
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value });
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleImageChange(event) {
        const value = event.target.value;
        this.setState({ image: value });
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ selectedLocation: value });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create New Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.fabric}
                                    onChange={this.handleFabricChange}
                                    placeholder="Fabric"
                                    required
                                    type="text"
                                    name="fabric"
                                    id="fabric"
                                    className="form-control"
                                />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.style}
                                    onChange={this.handleStyleChange}
                                    placeholder="Style"
                                    required
                                    type="text"
                                    name="style"
                                    id="style"
                                    className="form-control"
                                />
                                <label htmlFor="style">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.color}
                                    onChange={this.handleColorChange}
                                    placeholder="Color"
                                    required
                                    type="text"
                                    name="color"
                                    id="color"
                                    className="form-control"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.image}
                                    onChange={this.handleImageChange}
                                    placeholder="Image URL"
                                    required
                                    type="url"
                                    name="image"
                                    id="image"
                                    className="form-control"
                                />
                                <label htmlFor="image">Image URL</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select
                                    value={this.state.selectedLocation}
                                    onChange={this.handleLocationChange}
                                    required
                                    name="location"
                                    id="location"
                                    className="form-select"
                                >
                                    <option value="">Choose Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.import_href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                                <button className="btn btn-dark">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HatsForm;
