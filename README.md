# Wardrobify

Team:

* Kai Martin - Hats microservice
* Allison Bradshaw - Shoes microservice

## Design

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.


@Allison Bradshaw
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.


poll.py:
    -wardrobe api
        -service poller http://wardrobe-api:8000

Model.py:
    -Shoes Model
        -name
        -manufacturer
        -color
        -picture url
        -bin

views.py:
    -shoes list
    -detail list

RESTful api:
    -Insomnia
        -shoes list
        -create shoe
        -delete shoe

React Components:
    -list of all shoes
    -details of all shoes
    -form to create new shoe

Delete:
    -delete a shoe

Navigation:
    -route nav links to components
